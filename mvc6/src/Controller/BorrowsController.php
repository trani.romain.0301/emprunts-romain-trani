<?php

namespace App\Controller;

use Core\Kernel\AbstractController;

/**
 *
 */
class BorrowsController extends AbstractController
{
    public function borrows()
    {
        $borrows = 'Empruntes';
        //$this->dump($message1);
        $this->render('app.borrows.borrows',array(
            'borrows' => $borrows,
        ),'admin');
    }
}
