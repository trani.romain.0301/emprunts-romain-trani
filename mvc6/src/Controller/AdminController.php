<?php

namespace App\Controller;

use Core\Kernel\AbstractController;

/**
 *
 */
class AdminController extends AbstractController
{
    public function adminindex()
    {
        $message1 = 'Bienvenue sur l\'Admin';
        //$this->dump($message1);
        $this->render('app.emprunts.adminhome',array(
            'message1' => $message1,
        ),'admin');
    }
}
