<?php

namespace App\Controller;

use App\Model\AbonnesModel;
use Core\Kernel\AbstractController;
use App\Service\Validation;
use Core\App;
use App\Service\Form;


/**
 *
 */
class AbonnesController extends AbstractController
{


    public function abonnes()
    {
        $abonnes1 = 'Abonnés';
        //$this->dump($message1);
        $this->render('app.abonnes.abonnes',array(
            'abonnes' => AbonnesModel::getAllRecipeOrderBy(),
            'abonnes1' => $abonnes1,
        ),'admin');
    }

    public function single($id) {
        $abonne = $this->getRecipeByIdOr404($id);
        $abonne = AbonnesModel::findById($abonne->id);
        $this->render('app.abonnes.single',array(
            'abonne' => $abonne,
        ), 'admin');
    }

    public function add()
    {
        $errors = array();
        if(!empty($_POST['submitted'])) {
            $post = $this->cleanXss($_POST);
            $v = new Validation();
            $errors = $this->validate($v,$post);
            if($v->isValid($errors)) {
                AbonnesModel::insert($post);
                $this->addFlash('success', 'Merci de vous être inscritption !');
                $this->redirect('');
            }
        }
        $form = new Form($errors);
        $this->render('app.abonnes.add', array(
            'form' => $form,
        ), 'admin');
    }

    public function delete($id) {
        $this->getRecipeByIdOr404($id);
        AbonnesModel::delete($id);
        $this->addFlash('success', 'Abonné supprimer');
        $this->redirect('');
    }

    public function edit($id)
    {
        $abonne = $this->getRecipeByIdOr404($id);
        $errors = [];
        if(!empty($_POST['submitted'])) {
            $post = $this->cleanXss($_POST);
            $v = new Validation();
            $errors = $this->validate($v,$post);
            if($v->isValid($errors))  {
                AbonnesModel::update($id,$post);
                $this->addFlash('success', 'Abonné modfier');
                $this->redirect('');
            }
        }
        $form = new Form($errors);
        $this->render('app.abonnes.edit', array(
            'form' => $form,
            'abonne' => $abonne,
        ), 'admin');
    }





    private function getRecipeByIdOr404($id)
    {
        $abonne = AbonnesModel::findById($id);
        if(empty($abonne)) {
            $this->Abort404();
        }
        return $abonne;
    }

    private function validate($v, $post)
    {
        $errors = array();
        $errors['nom'] = $v->textValid($post['nom'], 'nom', 3, 100);
        $errors['prenom'] = $v->textValid($post['prenom'], 'prenom', 5, 500);
        $errors['email'] = $v->textValid($post['email'], 'email', 5, 255);
        return $errors;
    }

}
