<?php

namespace App\Controller;

use App\Model\ProductsModel;
use Core\Kernel\AbstractController;
use App\Service\Validation;
use Core\App;
use App\Service\Form;

/**
 *
 */
class ProductsController extends AbstractController
{
    public function products()
    {
        $products1 = 'Produits';
//        $this->dump($products);
        $this->render('app.products.products',array(
            'products' => ProductsModel::getAllRecipeOrderBy(),
            'products1' => $products1,
        ),'admin');
    }

    public function singlet($id) {
        $product = $this->getRecipeByIdOr404($id);
        $product = ProductsModel::findById($product->id);
        $this->render('app.products.singlet',array(
            'product' => $product,
        ), 'admin');
    }

    public function add()
    {
        $errors = array();
        if(!empty($_POST['submitted'])) {
            $post = $this->cleanXss($_POST);
            $v = new Validation();
            $errors = $this->validate($v,$post);
            if($v->isValid($errors)) {
                ProductsModel::insert($post);
                $this->addFlash('success', 'Merci d\'avoir ajouter un produit !');
                $this->redirect('');
            }
        }
        $form = new Form($errors);
        $this->render('app.products.add', array(
            'form' => $form,
        ), 'admin');
    }

    public function delete($id) {
        $this->getRecipeByIdOr404($id);
        ProductsModel::delete($id);
        $this->addFlash('success', 'Produit supprimer');
        $this->redirect('');
    }

    public function edit($id)
    {
        $product = $this->getRecipeByIdOr404($id);
        $errors = [];
        if(!empty($_POST['submitted'])) {
            $post = $this->cleanXss($_POST);
            $v = new Validation();
            $errors = $this->validate($v,$post);
            if($v->isValid($errors))  {
                ProductsModel::update($id,$post);
                $this->addFlash('success', 'Produit modfier');
                $this->redirect('');
            }
        }
        $form = new Form($errors);
        $this->render('app.products.edit', array(
            'form' => $form,
            'product' => $product,
        ), 'admin');
    }

    public function perso() {
//        $this->dump($products);
        $nbrproducts = ProductsModel::count();
        $this->render('app.emprunts.adminhome',array(
            'nbrproducts' => $nbrproducts,
        ),'admin');
    }






    private function getRecipeByIdOr404($id)
    {
        $product = ProductsModel::findById($id);
        if(empty($product)) {
            $this->Abort404();
        }
        return $product;
    }

    private function validate($v, $post)
    {
        $errors = array();
        $errors['titre'] = $v->textValid($post['titre'], 'titre', 3, 100);
        $errors['reference'] = $v->textValid($post['reference'], 'reference', 5, 500);
        $errors['description'] = $v->textValid($post['description'], 'decription', 5, 255);
        return $errors;
    }
}
