<?php
namespace App\Model;

use Core\App;
use Core\Kernel\AbstractModel;

class ProductsModel extends AbstractModel
{
    protected static $table = 'products';

    protected $id;
    protected $titre;
    protected $reference;
    protected $description;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getTitre()
    {
        return $this->titre;
    }

    /**
     * @return mixed
     */
    public function getReference()
    {
        return $this->reference;
    }

    /**
     * @return mixed
     */
    public function getDescription()
    {
        return $this->description;
    }

    public static function insert($post)
    {
        App::getDatabase()->prepareInsert(
            "INSERT INTO " . self::$table . " (titre, reference, description) VALUES (?,?,?)",
            array($post['titre'], $post['reference'], $post['description'])
        );
    }

    public static function delete($id,$columId = 'id')
    {
        return App::getDatabase()->prepareInsert("DELETE FROM " . self::$table . " WHERE ".$columId." = ?",[$id],get_called_class(),true);
    }

    public static function getAllRecipeOrderBy($column = 'titre', $order ='ASC')
    {
        return App::getDatabase()->query("SELECT * FROM " .self::getTable() . " ORDER BY $column $order",get_called_class());
    }

    public static function update($id,$post)
    {
        App::getDatabase()->prepareInsert(
            "UPDATE " . self::$table . " SET titre = ?, reference = ?, description = ? WHERE id = ?",
            array($post['titre'], $post['reference'], $post['description'], $id)
        );
    }

    public static function count()
    {
        return App::getDatabase()->aggregation("SELECT COUNT(id) FROM " . self::getTable());
    }

}