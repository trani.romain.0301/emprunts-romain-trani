<?php

$routes = array(
    array('home','default','index'),
// ADMIN
    array('adminhome','admin','adminindex'),
    array('abonnes','abonnes','abonnes'),
    array('products','products','products'),
    array('borrows','borrows','borrows'),
// CRUD 1
    array('single','abonnes','single', array('id')),
    array('add-abonnes','abonnes','add'),
    array('delete-abonnes','abonnes','delete', array('id')),
    array('edit-abonnes','abonnes','edit', array('id')),

// CRUD 2
    array('singlet','products','singlet', array('id')),
    array('add-products','products','add'),
    array('delete-products','products','delete', array('id')),
    array('edit-products','products','edit', array('id')),
);









