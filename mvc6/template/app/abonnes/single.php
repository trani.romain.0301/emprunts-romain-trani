<div class="container2">
    <h1>Fiche de description de l'abonné</h1>
    <label for="nom">Nom :</label>
    <p id="nom"><?php echo strtoupper($abonne->nom); ?></p>

    <label for="prenom">Prénom :</label>
    <p id="prenom"><?php echo strtolower($abonne->prenom); ?></p>

    <label for="email">Email :</label>
    <p id="email"><?php echo strtolower($abonne->email); ?></p>

    <label for="age">Âge :</label>
    <p id="age"><?php echo $abonne->age; ?></p>
</div>