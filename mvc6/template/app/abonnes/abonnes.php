<h1 style="text-align: center;font-size:33px;margin: 0;color:black;">
    <?= $abonnes1; ?>
</h1>
<div>
    <p><a class="btn" href="<?php echo $view->path('add-abonnes'); ?>">Ajouter un abonnée</a></p>
</div>
<?php
//$view->dump($abonnes); ?>
   <table>
      <thead>
        <tr>
          <th>Nom</th>
          <th>Prénom</th>
          <th>E-mail</th>
          <th>Âge</th>
          <th>Détails</th>
          <th>Supprimer</th>
          <th>Modifier</th>
        </tr>
      </thead>
       <?php foreach ($abonnes as $abonne) { echo '
      <tbody>
      <tr>
          <td>'.strtoupper($abonne->nom).'</td>
          <td>'.strtolower($abonne->prenom).'</td>
          <td>'.strtolower($abonne->email).'</td>
          <td>'.$abonne->age.'</td>
          <td><a href="' .$view->path('single', array('id' => $abonne->id)).'">Voir plus</a></td>        
          <td><a onclick="return confirm(\'Voulez-vous effacer ?\')" href="' .$view->path('delete-abonnes', array('id' => $abonne->id)).'">Effacer</a></td>
          <td><a href="' .$view->path('edit-abonnes', array('id' => $abonne->id)).'">Editer</a></td>
        </tr>
      </tbody>';
      }?>
    </table>
