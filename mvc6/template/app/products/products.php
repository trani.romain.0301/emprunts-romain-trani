<h1 style="text-align: center;font-size:33px;margin: 5px 0;color:black;">
    <?= $products1; ?>
</h1>
<div>
    <p><a class="btn" href="<?php echo $view->path('add-products'); ?>">Ajouter un produit</a></p>
</div>
<table>
    <thead>
    <tr>
        <th>Titre</th>
        <th>Reference</th>
        <th>Description</th>
        <th>Détails</th>
        <th>Supprimer</th>
        <th>Modifier</th>
    </tr>
    </thead>
    <?php foreach ($products as $product) { echo '
      <tbody>
      <tr>
          <td>'.strtoupper($product->titre).'</td>
          <td>'.strtoupper($product->reference).'</td>
          <td>'.strtolower($product->description).'</td>
          <td><a href="' .$view->path('singlet', array('id' => $product->id)).'">Voir plus</a></td>        
          <td><a onclick="return confirm(\'Voulez-vous effacer ?\')" href="' .$view->path('delete-products', array('id' => $product->id)).'">Effacer</a></td>
          <td><a href="' .$view->path('edit-products', array('id' => $product->id)).'">Editer</a></td>
        </tr>
      </tbody>';
    }?>
</table>
