<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Association</title>
    <?php echo $view->add_webpack_style('admin'); ?>
</head>
<body>
<?php // $view->dump($view->getFlash())?>

<header class="header">
    <div>
        <a id="logo" style="display: block" href="<?= $view->path(''); ?>"><img width="100%" src="<?= $view->asset('logo/logo.png'); ?>" alt="logo"></a>
    </div>
    <ul>
        <li><a href="<?= $view->path('adminhome'); ?>">Accueil</a></li>
        <li><a href="<?= $view->path('abonnes'); ?>">Abonnés</a></li>
        <li><a href="<?= $view->path('products'); ?>">Produits</a></li>
        <li><a href="<?= $view->path('borrows'); ?>">Emprunte</a></li>
    </ul>
</header>

<div class="container">
    <?= $content; ?>
</div>

<?php echo $view->add_webpack_script('admin'); ?>
</body>
</html>
